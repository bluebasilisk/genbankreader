/*
 * Copyright (c) 2015 Lonneke Scheffer [lonsch96@live.nl].
 * All rights reserved.
 *
 */

package genbank_reader;

import java.util.HashMap;

/**
 * This enum represents the orientation of a sequence element on a DNA sequence.
 * The sequence element can have a forward or a reverse orientation.
 * The way the DNA sequence of a sequence element is calculated also depends on the orientation, 
 * so this enum contains methods to do so.
 *
 * @author Lonneke Scheffer [lonsch96@live.nl]
 * @version 1.0
 */
public enum Orientation {
    /** The sequence element lies on the forward strand. */
    FORWARD,
    /** The sequence element lies on the reverse strand. */
    REVERSE {
        /**
         * Like the original method, but returns the reversed complement
         * sequence.
         */
        @Override
        public String getSubSequence(final String origin, final CoordinatePair coordinatePair) {
            String complementSequence = new String();

            // Get the string on the forward strand, given the CoordinatePair
            String forwardSequence = FORWARD.getSubSequence(origin, coordinatePair);

            for (int i = 0; i < forwardSequence.length(); i++) {
                // translate the current nucleotide to it's complement,
                // add this complement to the beginning of the new sequence
                complementSequence = COMPLEMENT_NUCLEOTIDES.get(forwardSequence.charAt(i) + "") + complementSequence;
            }

            return complementSequence;
        }
    };

    /** Contains the complement nucleotides of DNA. */
    private static final HashMap COMPLEMENT_NUCLEOTIDES = new HashMap();
    static {
        COMPLEMENT_NUCLEOTIDES.put("A", "T");
        COMPLEMENT_NUCLEOTIDES.put("T", "A");
        COMPLEMENT_NUCLEOTIDES.put("C", "G");
        COMPLEMENT_NUCLEOTIDES.put("G", "C");
        COMPLEMENT_NUCLEOTIDES.put("a", "t");
        COMPLEMENT_NUCLEOTIDES.put("t", "a");
        COMPLEMENT_NUCLEOTIDES.put("c", "g");
        COMPLEMENT_NUCLEOTIDES.put("g", "c");
        COMPLEMENT_NUCLEOTIDES.put("N", "N");
        COMPLEMENT_NUCLEOTIDES.put("n", "n");
    }

    /**
     * Takes the origin DNA sequence and a set of coordinates, and creates a
     * subsequence using these coordinates.
     *
     * @param origin         total origin sequence of the GenBank file
     * @param coordinatePair coordinates of the subsequence
     * @return               subsequence of the origin following the coordinates
     * @throws IllegalArgumentException if the coordinates lie outside the
     *                                  origin sequence
     */
    public String getSubSequence(final String origin,
                                 final CoordinatePair coordinatePair) {
        int beginIndex = coordinatePair.getStart() - 1;
        int endIndex = coordinatePair.getStop();

        if (endIndex > origin.length()) {
            throw new IndexOutOfBoundsException("Your gene coordinates are outside the range of your origin sequence! "
                    + "\nYour coordinates were: " + beginIndex + ".." + endIndex + ", "
                    + "but your origin sequence is only " + origin.length() + " characters long!");
        }

        return origin.substring(beginIndex, endIndex);
    }
}