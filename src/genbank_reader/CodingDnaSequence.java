/*
 * Copyright (c) 2015 Lonneke Scheffer [lonsch96@live.nl].
 * All rights reserved.
 *
 */

package genbank_reader;

import java.util.List;

/**
 * This class represents a Coding DNA Sequence, which is a Sequence Element of a GenBank file.
 * It holds the CDS coordinates, orientation, product, protein ID and translation.
 * It has methods to obtain information from the Coding DNA Sequence in various formats.
 * 
 * @author Lonneke Scheffer [lonsch96@live.nl]
 * @version 1.0
 */
public class CodingDnaSequence extends SequenceElement {
    /** The product name of the CDS. */
    private final String product;
    /** The protein ID of the CDS. */
    private final String proteinId;
    /** The translation of the CDS to amino acid sequence. */
    private final String translation;

    /**
     * Create a new coding DNA sequence, given the coordinates, orientation
     * product name, protein ID and amino acid translation.
     *
     * @param coordinates a list of coordinate pairs
     * @param orientation the orientation of the CDS (forward/reverse)
     * @param product     the product name of the CDS
     * @param proteinId   the protein ID of the CDS
     * @param translation the amino acid translation of the CDS
     */
    public CodingDnaSequence(final List<CoordinatePair> coordinates,
                            final Orientation orientation,
                            final String product,
                            final String proteinId,
                            final String translation) {

        super(coordinates, orientation);
        this.product = product;
        this.proteinId = proteinId;
        this.translation = translation;
    }

    @Override
    public final String getFastaHeader() {
        return ">CDS " + this.getProduct() + " sequence" + System.lineSeparator();
    }

    @Override
    public final String getFeatureDetails() {
        CoordinatePair outerCoordinates = this.getOuterCoordinates();
        return this.getProduct() + ";CDS;" + outerCoordinates.getStart() + ";" + outerCoordinates.getStop()
                + ";" + this.getOrientation().toString().charAt(0);
    }

    @Override
    public final String toString() {
        return "CDS\nproduct: " + this.getProduct()
                + "\nprotein id: " + this.getProteinId()
                + "\norientation: " + this.getOrientation().toString().toLowerCase()
                + "\nouter coordinates: " + this.getOuterCoordinates().toString()
                + "\ntranslation:\n" + this.getTranslation();
    }

    /**
     * Gets the product name of the CDS.
     *
     * @return product name
     */
    public final String getProduct() {
        return this.product;
    }

    /**
     * Gets the protein ID of the CDS.
     *
     * @return protein id
     */
    public final String getProteinId() {
        return this.proteinId;
    }

    /**
     * Gets the amino acid translation of the CDS.
     *
     * @return amino acid translation
     */
    public final String getTranslation() {
        return this.reformatSequence(this.translation);
    }
}
