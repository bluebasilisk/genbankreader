/*
 * Copyright (c) 2015 Lonneke Scheffer [lonsch96@live.nl].
 * All rights reserved.
 *
 */

package genbank_reader;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class represents the data from a GenBank file.
 * It contains all the information needed to solve the use cases:
 * filename, accession, source organism, origin sequence and lists of CDSs and genes.
 * It also contains methods to solve all use cases.
 *
 * @author Lonneke Scheffer [lonsch96@live.nl]
 * @version 1.0
 */
public class GenBankData {
    /** The name of the genbank file. */
    private final String filename;
    /** Accession number of the genbank file. */
    private final String accession;
    /** The source organism of the DNA sequence. */
    private final String sourceOrganism;
    /** The total DNA sequence (origin) of the genbank file, in uppercase. */
    private final String origin;
    /** A list of all CDS elements in the genbank file. */
    private final List<CodingDnaSequence> codingSequences;
    /** A list of all gene elements in the genbank file. */
    private final List<Gene> genes;

    /**
     * Creating a new GenBankData.
     *
     * @param filename        the name of the genbank file
     * @param accession       the accession number of the genbank file
     * @param sourceOrganism  the organism of the DNA in the genbank file
     * @param codingSequences list of all CDS elements in the genbank file
     * @param genes           list of all gene elements in the genbank file
     * @param origin          total origin sequence of the genbank file
     */
    public GenBankData(final String filename, final String accession, final String sourceOrganism,
                       final List<CodingDnaSequence> codingSequences, final List<Gene> genes, final String origin) {
        this.filename = filename;
        this.accession = accession;
        this.sourceOrganism = sourceOrganism;
        this.codingSequences = codingSequences;
        this.genes = genes;
        this.origin = origin.toUpperCase();
    }

    /**
     * Creates a textual summary of the parsed file.
     * containing:
     *  - the filename
     *  - the organism name
     *  - the accession number
     *  - the length of the origin sequence
     *  - the number of genes
     *  - the forward/reverse balance of the genes
     *  - the number of CDSs
     *
     * @return String with textual summary
     */
    public final String getSummary() {
        // Count the genes with orientation 'forward'
        double forwardGenes = 0;
        for (Gene gene : this.getGenes()) {
            if (gene.getOrientation().equals(Orientation.FORWARD)) {
                forwardGenes += 1;
            }
        }
        return    "file              " + this.getFilename() + System.lineSeparator()
                + "organism          " + this.getSourceOrganism() + System.lineSeparator()
                + "accession         " + this.getAccession() + System.lineSeparator()
                + "sequence length   " + this.getOrigin().length() + " bp" + System.lineSeparator()
                + "number of genes   " + this.getGenes().size() + System.lineSeparator()
                + "gene F/R balance  " + forwardGenes / this.getGenes().size() + System.lineSeparator()
                + "number of CDSs    " + this.getCodingSequences().size();
    }

    /**
     * Returns nucleotide sequences of the genes that match the given
     * gene name pattern, in FASTA format.
     *
     * @param  pattern gene name pattern
     * @return         DNA FASTA representation of the matching genes
     */
    public final String fetchGeneFasta(final String pattern) {
        String fasta = new String();

        // For all genes: check if the gene name matches the given pattern
        Pattern compiledPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        for (Gene gene : this.getGenes()) {
            Matcher match = compiledPattern.matcher(gene.getGeneName());
            if (match.find()) {
                // If the pattern matches, add the gene to the FASTA output string
                if (!fasta.isEmpty()) {
                    fasta += System.lineSeparator();
                }
                fasta += gene.getFastaHeader();
                fasta += gene.getDnaSequence(this.getOrigin());
            }
        }
        return fasta;
    }

    /**
     * Returns the amino acid sequences of the CDSs that match the given
     * product name pattern, in FASTA format.
     *
     * @param  pattern product name pattern
     * @return         amino acid FASTA representation of the matching CDSs
     */
    public final String fetchCdsFasta(final String pattern) {
        String fasta = new String();

        // For all CDSs: check if the CDS product name matches the given pattern
        Pattern compiledPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        for (CodingDnaSequence cds : this.codingSequences) {
            Matcher match = compiledPattern.matcher(cds.getProduct());
            if (match.find()) {
                // If the pattern matches, add the CDS to the FASTA output string
                if (!fasta.isEmpty()) {
                    fasta += System.lineSeparator();
                }
                fasta += cds.getFastaHeader() + cds.getTranslation();
            }
        }

        return fasta;
    }

    /**
     * Returns all features (genes and CDSs) with name, type, start, stop and
     * orientation between the given coordinates.
     * Only features that are completely covered on the given region are listed.
     *
     * @param  region coordinates of the features to be listed
     * @return        string representation of the features, listed like:
     *                FEATURE;TYPE;START;STOP;ORIENTATION
     */
    public final String fetchFeatures(final CoordinatePair region) {

        String features = "FEATURE;TYPE;START;STOP;ORIENTATION";

        // Create a list containing the two lists of features (genes & CDSs)
        List<List> allFeatures = new LinkedList();
        allFeatures.add(this.getGenes());
        allFeatures.add(this.getCodingSequences());

        // for both lists..
        for (List<SequenceElement> sequenceElements : allFeatures) {
            // for the SequenceElement (= gene or CDS) in the list..
            for (SequenceElement element : sequenceElements) {
                // If the given region contains the whole gene/CDS, add it to the 'features' string
                if (region.containsCoordinates(element.getOuterCoordinates())) {
                    features += System.lineSeparator();
                    features += element.getFeatureDetails();
                }
            }
        }

        return features;
    }

    /**
     * Lists the locations of all the sites where the DNA pattern is found:
     * position and actual sequence and (if relevant) the gene in which it resides.
     *
     * @param givenPattern The IUPAC pattern to match
     * @return             a list with POSITION;SEQUENCE;GENE for all occurrences
     */
        public final String findSites(final String givenPattern) {
        IupacPattern iupacPattern  = new IupacPattern(givenPattern);
        String allSites = "POSITION;SEQUENCE;GENE";
        String newSites = "";

        System.out.println("site search: " + iupacPattern.getPattern() + " (regex: " + iupacPattern.getRegex() + ")");

        Pattern compiledPattern = Pattern.compile(iupacPattern.getRegex());
        Matcher match = compiledPattern.matcher(this.getOrigin());

        while (match.find()) {
            // gain all genes at this match position, add them to newSites
            for (Gene gene : this.getGenes()) {
                // If the match falls between the outer coordinates of the gene: add the gene to newSites
                // (if the match falls in an intron: still add the gene to newSites, because an intron is an
                // INTRAgenic region, not an INTERgenic region)
                if (gene.getOuterCoordinates().containsCoordinates(match.start(), match.end())) {
                    newSites += System.lineSeparator() + (match.start() + 1) + ";" + match.group() + ";"
                             + gene.getGeneName();
                }
            }
            // if no genes were added to newSites, add the match with 'INTERGENIC' as 'GENE'
            if (newSites.isEmpty()) {
                newSites += System.lineSeparator() + (match.start() + 1) + ";" + match.group() + ";INTERGENIC";
            }
            allSites += newSites;
            newSites = "";
        }
        return allSites;
    }

    @Override
    public String toString() {
        return "file:      " + this.getFilename() + System.lineSeparator()
             + "organism:  " + this.getSourceOrganism() + System.lineSeparator()
             + "accession: " + this.getAccession() + System.lineSeparator();
    }

    /**
     * Gets the accession number of the GenBank file.
     *
     * @return accession number
     */
    public final String getAccession() {
        return this.accession;
    }

    /**
     * Gets the source organism of the DNA in the GenBank file.
     *
     * @return source organism
     */
    public final String getSourceOrganism() {
        return this.sourceOrganism;
    }

    /**
     * Gets a list of the CDS elements in the GenBank file.
     *
     * @return list of CodingDnaSequences
     */
    public final List<CodingDnaSequence> getCodingSequences() {
        return this.codingSequences;
    }

    /**
     * Gets a list of the gene elements in the GenBank file.
     *
     * @return list of Genes
     */
    public final List<Gene> getGenes() {
        return this.genes;
    }

    /**
     * Gets the full origin DNA sequence of the GenBank file.
     *
     * @return origin DNA sequence
     */
    public final String getOrigin() {
        return this.origin;
    }

    /**
     * Gets the original file name of the GenBank file.
     *
     * @return filename
     */
    public String getFilename() {
        return this.filename;
    }
}
