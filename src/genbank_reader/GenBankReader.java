/*
 * Copyright (c) 2015 Lonneke Scheffer [lonsch96@live.nl].
 * All rights reserved.
 *
 */

package genbank_reader;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * This class is the main controller in the GenBank reader package.
 * It contains the main() method, it parses the user input, extracts the useful data
 * and controls the use cases.
 *
 * @author Lonneke Scheffer [lonsch96@live.nl]
 * @version 1.0
 */
public final class GenBankReader {
    /** A CommandLine object containing the parsed command line. */
    private CommandLine parsedCommandLine;
    /** The scanner to scan over the given input data.  */
    private Scanner inputData;
    /** The file name of the given GenBank file. */
    private String filename;
    /** The list of CDSs found in the given GenBank file. */
    private final List<CodingDnaSequence> codingSequences;
    /** The list of the genes found in the given GenBank file. */
    private final List<Gene> genes;
    /** The origin sequence of the given GenBank file. */
    private String origin;
    /** The GenBankData object created to store the important information of the given GenBank file. */
    private GenBankData genBankData;

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        GenBankReader mainObject = new GenBankReader();
        mainObject.start(args);
    }

    /**
     * private constructor.
     */
    private GenBankReader() {
        this.genes = new LinkedList();
        this.codingSequences = new LinkedList();
    }

    /**
     * Starts the application.
     *
     * @param args the command line arguments passed from main()
     */
    private void start(final String[] args) {
        this.parseCommandLine(args);
        this.parseGenBankFile();
        this.printOutput();
        
                // overal this. gebruiken bij aanroepen van methodes/variabelen

        // overal (toString) implementeren
    }

    /**
     * Parses the command line arguments using the  Apache Commons CLI library.
     * Saves the parsed command line as a CommandLine object to this.parsedCommandLine
     *
     * @param args the command line arguments passed from start()
     */
    private void parseCommandLine(final String[] args) {
        Options options = new Options();
        HelpFormatter formatter = new HelpFormatter();
        CommandLineParser parser = new DefaultParser();

        options.addOption("h", "help", false, "print this message");
        options.addOption("i", "infile", true, "input Genbank file");
        options.addOption("s", "summary", false, "print a summary of the given Genbank file");
        options.addOption("g", "fetch_gene", true, "fetch all genes with the given name RegEx pattern");
        options.addOption("c", "fetch_cds", true, "fetch all CDSs with the given product name RegEx pattern");
        options.addOption("f", "fetch_features", true, "fetch all features between the given coordinates like: 0..100");
        options.addOption("t", "find_sites", true, "find all sites where the DNA matches the given IUPAC code");

        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption("help")) {
                // If the user called '-h' or '--help': show the help and do not continue.
                formatter.printHelp("java -jar GenBankReader.jar --infile <INFILE> [--summary]  "
                            + "[--fetch_gene <GENE NAME PATTERN>] [--fetch_cds <PRODUCT NAME PATTERN>] "
                            + "[--fetch_features <COORDINATES>] [--find_sites <DNA SEQ WITH IUPAC CODES>]"
                            + "[--help]", options);
                System.exit(0);
            } else if (!cmd.hasOption("infile")) {
                // If no GenBank file was provided: show this message and quit
                System.out.println("Please support an input genbank file, "
                        + "or run with the parameter -h for more detailed help.");
                System.exit(0);
            }

            this.parsedCommandLine = cmd;
        } catch (ParseException ex) {
            // If, for some reason, the command line could not be parsed: print the exception message and quit.
            System.out.println(ex.getMessage());
            System.exit(0);
        }
    }

    /**
     * Parses the given GenBankFile.
     * Uses the 'infile' field of this.parsedCommandLine to obtain the input file,
     * parses the file, saves the data in this.genBankData as a new GenBankData object.
     */
    private void parseGenBankFile() {
        String accession;
        String sourceOrganism;

        // Obtain the 'infile' from the parsedCommandLine, and create a java.io.File from it
        java.io.File inputFile = new java.io.File(this.parsedCommandLine.getOptionValue("infile"));
        this.filename = inputFile.getName();

        // To prevent the usage of invalid files: quit the program if it does not have the right extension (*.gb/*.gbk)
        if (!(this.filename.endsWith(".gbk") || this.filename.endsWith(".gb"))) {
            System.out.println("WARNING: Your input file '" + this.filename + "' "
                    + "does not have a GenBank extension (*.gb or *.gbk)."
                    + "\n\t Please try again with a valid GenBank file.");
            System.exit(0);
        }

        try {
            // Use a Scanner object to parse the input file
            this.inputData = new Scanner(inputFile);

            // Find the lines that start with ACCESSION/ORGANISM, then save the string from character 12
            // This cuts the 'ACCESSION   ' and '  ORGANISM  ' off the beginning of the string
            accession = this.inputData.findWithinHorizon("ACCESSION   .+", 0).substring(12);
            sourceOrganism = this.inputData.findWithinHorizon("  ORGANISM  .+", 0).substring(12);

            // Jump to the 'FEATURES' part of the GenBank file, then process the Features and Origin
            this.inputData.findWithinHorizon("FEATURES.+", 0);
            this.parseFeatures();
            this.parseOrigin();

            // At last, create a new GenBankData object from the obtained information
            this.genBankData = new GenBankData(this.filename, accession, sourceOrganism,
                                               this.codingSequences, this.genes, this.origin);
        } catch (FileNotFoundException e) {
            // Print a warning if the file does not exist, then quit.
            System.out.println("WARNING: Your input file '" + filename + "' can not be found."
                    + "\n\t Please try again with a different file.");
            System.exit(0);
        }
    }

    /**
     * Parses the 'FEATURES' part of the given GenBankFile.
     * Extracts all 'gene' and 'CDS' blocks from the GenBankFile, saves the information to Gene and CodingSequence
     * objects, and adds these objects to the lists this.genes and this.codingSequences
     */
    private void parseFeatures() {
        boolean inOrigin = false;
        boolean inGene = false;
        boolean inCds = false;
        String line;
        String geneInfo = "";
        String cdsInfo = "";

        while (this.inputData.hasNext() && !inOrigin) {
            line = this.inputData.nextLine();

            // To keep track of 'where' a line is (in a gene block, CDS block, origin) boolean values are used
            if (inGene) {
                // Collect all information within the 'gene' block in string geneInfo
                if (line.startsWith("                     /")) {
                    geneInfo += line;
                } else {
                    inGene = false;
                }
            } else if (inCds) {
                // Do the same for CDS
                if (line.startsWith("                     ")) {
                    cdsInfo += line;
                } else {
                    inCds = false;
                }
            }

            if (line.startsWith("ORIGIN")) {
                inOrigin = true;
            } else if (line.startsWith("     gene")) {
                // If a new gene is encountered, parse the last gene before overwriting geneInfo
                if (!geneInfo.equals("")) {
                    this.genes.add(this.parseLastGene(geneInfo));
                }
                geneInfo = line;
                inGene = true;
            } else if (line.startsWith("     CDS")) {
                // Do the same for CDS
                if (!cdsInfo.equals("")) {
                    this.codingSequences.add(this.parseLastCds(cdsInfo));
                }
                cdsInfo = line;
                inCds = true;
            }
        }
        // Check if there were any genes/CDSs found before parsing the last one (to prevent 'empty' genes/CDSs)
        if (!geneInfo.equals("")) {
            this.genes.add(this.parseLastGene(geneInfo));
        }
        if (!cdsInfo.equals("")) {
            this.codingSequences.add(this.parseLastCds(cdsInfo));
        }
    }

    /**
     * Parses a String containing CDS information.
     * Extracts the CDS product, protein ID, translation, orientation and coordinates, and saves them
     * in a new CodingSequence object.
     *
     * @param cdsInfo string containing all lines within a CDS block in a GenBank file
     * @return        a new CodingSequence object
     */
    private CodingDnaSequence parseLastCds(final String cdsInfo) {
        String[] cdsFeatures;
        Orientation cdsOrientation = Orientation.FORWARD;
        List<CoordinatePair> cdsCoordinates;
        String product = "UNKNOWN";
        String proteinId = "UNKNOWN";
        String translation = "";
        boolean inTranslation = false;

        // remove the word CDS, multiple consecutive whitespace characters, and split at the '/'
        cdsFeatures = cdsInfo.replaceFirst("CDS", "").replaceAll("\\s{2,}", "").split("/");

        // If the coordinates (cdsFeatures[0]) contain the word 'complement': the orientation is reverse
        if (cdsFeatures[0].contains("complement")) {
            cdsOrientation = Orientation.REVERSE;
        }

        // Get all features (product, protein_id, translation, coordinates)
        for (String feature : cdsFeatures) {
            if (inTranslation) {
                translation += feature.replace("\"", "");
            } else if (feature.startsWith("product")) {
                product = feature.replaceFirst("product=", "").replace("\"", "");
            } else if (feature.startsWith("protein_id")) {
                proteinId = feature.replaceFirst("protein_id=", "").replace("\"", "");
            } else if (feature.startsWith("translation")) {
                translation = feature.replaceFirst("translation=", "").replace("\"", "");
            }
        }
        cdsCoordinates = getCoordinatesFromLine(cdsFeatures[0], "CDS " + proteinId);

        return new CodingDnaSequence(cdsCoordinates, cdsOrientation, product, proteinId, translation);
    }

    /**
     * Parses a String containing gene information.
     * Extracts the gene name (preferred) or gene locus, orientation and coordinates, and saves them
     * in a new Gene object.
     *
     * @param geneInfo string containing all lines within a gene block in a GenBank file
     * @return         a new Gene object
     *
     */
    private Gene parseLastGene(final String geneInfo) {
        String[] geneFeatures;
        Orientation geneOrientation = Orientation.FORWARD;
        List<CoordinatePair> geneCoordinates;
        String geneName = "UNKNOWN";

        // remove the word gene, multiple consecutive whitespace characters, and split at the '/'
        geneFeatures = geneInfo.replaceFirst("gene", "").replaceAll("\\s{2,}", "").split("/");

        // If the coordinates (geneFeatures[0]) contain the word 'complement': the orientation is reverse
        if (geneFeatures[0].contains("complement")) {
            geneOrientation = Orientation.REVERSE;
        }

        // Get all features (name(preferred) or locus, coordinates)
        for (String feature : geneFeatures) {
            if (feature.startsWith("gene")) {
                geneName = feature.replaceFirst("gene=", "").replace("\"", "");
                break;
            } else if (feature.startsWith("locus")) {
                geneName = feature.replaceFirst("locus=", "").replaceFirst("locus_tag=", "").replace("\"", "");
            }
        }
        geneCoordinates = getCoordinatesFromLine(geneFeatures[0], "gene " + geneName);

        return new Gene(geneCoordinates, geneOrientation, geneName);
    }

    /**
     * Parses the 'ORIGIN' part of the given GenBankFile.
     * Removes all numbers and whitespace, saves the origin DNA sequence to this.origin.
     * Prints a warning message if the file is a multi GenBank file, and ignores the second GenBank record.
     */
    private void parseOrigin() {
        boolean inFirstGenBank = true;
        String line;
        this.origin = "";

        while (inputData.hasNext() && inFirstGenBank) {
            line = inputData.nextLine();
            if (line.startsWith("LOCUS")) {
                // Second genbank record is encountered, stop the loop and report to the user.
                inFirstGenBank = false;
                System.out.println("WARNING: Your inputfile " + this.filename + " contains multiple genbank records."
                        + "\n\t Only the first record will be processed.");
            } else if (!line.equals("//")) {
                // add the line to the origin sequence, without the first 10 whitespace/number characters
                // (this is more efficient than scanning the whole line for numbers)
                // and without any other whitespace characters.
                this.origin += line.substring(10).replace(" ", "");
            }
        }
    }

    /**
     * Creates a list of CoordinatePairs from a given line.
     * The parameter 'description' is used to give a more useful error message if the input was invalid.
     *
     * @param line        the string to extract a sequence from
     * @param description a description of where the line comes from (gene/CDS/CLI)
     * @return            a (linked) list of CoordinatePairs
     */
    private List<CoordinatePair> getCoordinatesFromLine(final String line, final String description) {
        List<CoordinatePair> coordinateList = new LinkedList();
        String[] splittedLine;
        String[] splittedCoordinates;
        String simplifiedLine;
        Pattern legalCoordinates = Pattern.compile("(\\d+..\\d+,?)+");
        int firstCoordinate;
        int secondCoordinate;

        // create a simplified line with only numbers, '..' and comma's
        simplifiedLine = line.replace("complement", "").replace("join", "").replaceAll("[()<>]", "");

        // If the simplified line does not match the pattern, the line is illegal, and coordinates are set to 0..0
        // (Example of illegal line: '1000', '33m3..400', or an empty line)
        Matcher match = legalCoordinates.matcher(simplifiedLine);
        if (!match.matches()) {
            // 'description' is used to describe where the error occurred (which gene/CDS, or command line input)
            System.out.println("WARNING: Your coordinates '" + line + "' in '" + description
                                + "' are not legal, and have been set to 0..0");
            coordinateList.add(new CoordinatePair(0, 0));
            return coordinateList;
        } else {
            // The line is first splitted at ',' and later at '..' to obtain the actual integers
            splittedLine = simplifiedLine.split(",");
            for (String coordinateString : splittedLine) {
                splittedCoordinates = coordinateString.split("\\.\\.");
                firstCoordinate = Integer.parseInt(splittedCoordinates[0]);
                secondCoordinate = Integer.parseInt(splittedCoordinates[1]);

                if (firstCoordinate < secondCoordinate) {
                    coordinateList.add(new CoordinatePair(firstCoordinate, secondCoordinate));
                } else {
                    // If the lowest given coordinate was bigger than the biggest given coordinate,
                    // a warning (describing where it went wrong) is printed and the coordinates are switched.
                    System.out.println("WARNING: Your lowest coordinate in '" + description
                            + "' was bigger than your biggest coordinate: '" + coordinateString
                            + "'.\n\t This has been corrected to '" + secondCoordinate + ".." + firstCoordinate + "'.");
                    coordinateList.add(new CoordinatePair(secondCoordinate, firstCoordinate));
                }
            }
        return coordinateList;
        }
    }

    /**
     * Calls {@link GenBankReader#getCoordinatesFromLine(String, String)}
     * with the String description set to 'UNKNOWN'.
     *
     * @param line the string to extract a sequence from
     * @return     a (linked) list of CoordinatePairs
     */
    private List<CoordinatePair> getCoordinatesFromLine(final String line) {
        return this.getCoordinatesFromLine(line, "UNKNOWN");
    }

    /**
     * Prints the output of the selected use cases.
     * Checks which use cases were selected, checks if the right user input is given
     * and calls the use case methods of the created genBankData.
     */
    public void printOutput() {
        // Yes, I wish this method was shorter too... but that would make it less readable.
        // For all use cases there is checked if the user has selected that use case
        // If that is true, the use case method in the GenBankFile is called
        // If there is no output, a message is printed. Otherwise the method output is printed.
        if (this.parsedCommandLine.hasOption("summary")) {
            System.out.println(this.genBankData.getSummary());
        }
        if (this.parsedCommandLine.hasOption("fetch_gene")) {
            try {
                String genePattern = this.parsedCommandLine.getOptionValue("fetch_gene");
                String fetchGeneOutput = this.genBankData.fetchGeneFasta(genePattern);
                if (fetchGeneOutput.equals("")) {
                    System.out.println("No genes were found using the pattern: " + genePattern);
                } else {
                    System.out.println(fetchGeneOutput);
                }
            } catch (IndexOutOfBoundsException ex) {
                // Catch the IndexOutOfBoundsException that is thrown if a gene has coordinates outside the origin
                System.out.println("WARNING: " + ex.getMessage());
                System.out.println("Please check your GenBank file or try again with a different file");
            }
        }
        if (this.parsedCommandLine.hasOption("fetch_cds")) {
            String cdsPattern = this.parsedCommandLine.getOptionValue("fetch_cds");
            String fetchCdsOutput = this.genBankData.fetchCdsFasta(cdsPattern);
            if (fetchCdsOutput.equals("")) {
                System.out.println("No CDSs were found using the pattern: " + cdsPattern);
            } else {
                System.out.println(fetchCdsOutput);
            }
        }
        if (this.parsedCommandLine.hasOption("fetch_features")) {
            String userInput = this.parsedCommandLine.getOptionValue("fetch_features");
            CoordinatePair userCoordinates = getCoordinatesFromLine(userInput, "command line input").get(0);
            String fetchFeaturesOutput = this.genBankData.fetchFeatures(userCoordinates);
            if (fetchFeaturesOutput.equals("FEATURE;TYPE;START;STOP;ORIENTATION")) {
                System.out.println("No features were found using the coordinates: " + userCoordinates);
            } else {
                System.out.println(fetchFeaturesOutput);
            }
        }
        if (this.parsedCommandLine.hasOption("find_sites")) {
            try {
                String iupacSequence = this.parsedCommandLine.getOptionValue("find_sites");
                String findSitesOutput = this.genBankData.findSites(iupacSequence);
                if (findSitesOutput.equals("POSITION;SEQUENCE;GENE")) {
                    System.out.println("No sites were found using the IUPAC sequence: " + iupacSequence);
                } else {
                    System.out.println(findSitesOutput);
                }
            } catch (IllegalArgumentException ex) {
                // If the IUPAC code contains invalid characters, this IllegalArgumentException is thrown
                System.out.println("WARNING: " + ex.getMessage());
                System.out.println("Please try again with a different IUPAC code.");
            }
        }
    }
}

