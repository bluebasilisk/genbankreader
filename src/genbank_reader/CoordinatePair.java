/*
 * Copyright (c) 2015 Lonneke Scheffer [lonsch96@live.nl].
 * All rights reserved.
 *
 */

package genbank_reader;

/**
 * This class represents a coordinate pair.
 * It contains a start and a stop coordinate, and various methods to check
 * if the coordinate pair contains another coordinate pair or point in space.
 *
 * @author Lonneke Scheffer [lonsch96@live.nl]
 * @version 1.0
 */
public class CoordinatePair {
    /** The start position of the sequence element on the origin. */
    private final int start;
    /** The stop position of the sequence element on the origin. */
    private final int stop;

    /**
     * Create a new coordinate pair if the given start and stop coordinates are
     * valid. Start and stop coordinates are valid if they are above zero and
     * if the stop coordinate is not below the start coordinate.
     *
     * @param start position of the first nucleotide on the origin
     * @param stop  position of the last nucleotide on the origin
     * @throws IllegalArgumentException if the stop coordinate was below the
     *                                  start coordinate
     * @throws IllegalArgumentException if the start position was below zero
     */
    public CoordinatePair(final int start, final int stop) {
        if (start > stop) {
            throw new IllegalArgumentException("Your stop position '"
                    + stop + "' can not be below your start position '"
                    + start + "' !");
        // Don't need to check if stop < 0 because you know stop > start
        } else if (start < 0) {
            throw new IllegalArgumentException("Your start position '"
                    + start + "' was below zero!");
        }

        this.start = start;
        this.stop = stop;
    }

   /**
    * Checks if another coordinate pair falls between the ranges of this
    * coordinate pair.
    *
    * @param other the other coordinate pair
    * @return      boolean whether or not this coordinate pair contains
    *              the other (given) coordinate pair.
    */
    public final boolean containsCoordinates(final CoordinatePair other) {
        // The if statement 'is redundant', but this is more readable
        if (other.getStart() < this.getStart() || other.getStop() > this.getStop()) {
            return false;
        }
        return true;
    }

    /**
     * Calls {@link CoordinatePair#containsCoordinates(CoordinatePair)},
     * using the given integers to create a new CoordinatePair.
     *
     * @param firstCoordinate  the first coordinate integer
     * @param secondCoordinate the second coordinate integer
     * @return                 boolean whether or not this coordinate pair contains
     *                         the other (given) coordinate pair.
     */
    public final boolean containsCoordinates(final int firstCoordinate, final int secondCoordinate) {
        return this.containsCoordinates(new CoordinatePair(firstCoordinate, secondCoordinate));
    }

    /**
     * Checks if the given integer falls between the ranges of this coordinate pair.
     *
     * @param position integer to check
     * @return         boolean whether or not this coordinate pair contains the
     *                 (given) integer.
     */
    public final boolean containsPosition(final int position) {
        // The if statement 'is redundant', but this is more readable
        if (position < this.getStart() || position > this.getStop()) {
            return false;
        }
        return true;
    }

    @Override
    public final String toString() {
        return Integer.toString(this.getStart()) + ".." + Integer.toString(this.getStop());
    }

    /**
     * Gets the start position of the sequence element on the origin.
     *
     * @return start position
     */
    public final int getStart() {
        return start;
    }

    /**
     * Gets the stop position of the sequence element on the origin.
     *
     * @return stop position
     */
    public final int getStop() {
        return stop;
    }
}
