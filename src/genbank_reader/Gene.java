/*
 * Copyright (c) 2015 Lonneke Scheffer [lonsch96@live.nl].
 * All rights reserved.
 *
 */

package genbank_reader;

import java.util.List;

/**
 * This class represents a Gene, which is a Sequence Element of a GenBank file.
 * It holds the Gene coordinates, orientation, and gene name (preferred) or locus.
 * It has methods to obtain information from the Gene in various formats.
 *
 * @author Lonneke Scheffer [lonsch96@live.nl]
 * @version 1.0
 */
public class Gene extends SequenceElement {
    /** The gene name/locus. */
    private final String geneName;

    /**
     * Create a new gene, given the coordinates, orientation and name/locus.
     *
     * @param coordinates list of coordinates of the gene
     * @param orientation orientation (forward/reverse)
     * @param geneName   the name of the gene, or the locus if name is not present
     */
    public Gene(final List<CoordinatePair> coordinates,
                final Orientation orientation,
                final String geneName) {
        super(coordinates, orientation);
        this.geneName = geneName;
    }

    @Override
    public final String getFastaHeader() {
        return ">gene " + this.getGeneName() + " sequence" + System.lineSeparator();
    }

    @Override
    public final String getFeatureDetails() {
        return this.getGeneName() + ";gene;" + this.getOuterCoordinates().getStart() + ";"
                + this.getOuterCoordinates().getStop() + ";" + this.getOrientation().toString().charAt(0);
    }
    
    @Override
    public final String toString() {
        return "Gene\nname/locus: " + this.getGeneName()
                + "\norientation: " + this.getOrientation().toString().toLowerCase()
                + "\nouter coordinates: " + this.getOuterCoordinates().toString();
    }

    /**
     * Gets the gene name/locus.
     *
     * @return gene name/locus
     */
    public final String getGeneName() {
        return this.geneName;
    }
}
