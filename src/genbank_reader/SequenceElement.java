/*
 * Copyright (c) 2015 Lonneke Scheffer [lonsch96@live.nl].
 * All rights reserved.
 *
 */

package genbank_reader;

import java.util.List;

/**
 * This abstract class represents a Sequence Element of a GenBank file.
 * It holds the sequence element coordinates and orientation.
 * It contains methods to find the outer coordinates and DNA sequence.
 *
 * @author Lonneke Scheffer [lonsch96@live.nl]
 * @version 1.0
 */
public abstract class SequenceElement {
    /** A list of all coordinates of the sequence element on the origin. */
    private final List<CoordinatePair> coordinates;
    /** The orientation of the sequence element (forward or reverse). */
    private final Orientation orientation;
    /** The outer coordinates of the sequence element. */
    private final CoordinatePair outerCoordinates;

    /**
     * Creating a new sequence element, given the coordinates and orientation.
     *
     * @param coordinates a list of all coordinates on the origin
     * @param orientation the orientation (forward/reverse)
     */
    public SequenceElement(final List<CoordinatePair> coordinates,
                           final Orientation orientation) {
        // testen of coordinates geen lege list is
        this.coordinates = coordinates;
        this.orientation = orientation;
        this.outerCoordinates = this.calculateOuterCoordinates(coordinates);
    }

    /**
     * Determines the DNA sequence of the sequence element, given the origin
     * sequence and using the coordinates.
     *
     * @param origin the origin sequence of the genbank file
     * @return       the DNA sequence of the sequence element
     */
    public final String getDnaSequence(final String origin) {
        String dnaSequence = new String();

        // Determine the subsequence for every coordinate pair, concatenate those subsequences
        for (CoordinatePair coordinatePair : this.getCoordinates()) {
            dnaSequence += orientation.getSubSequence(origin, coordinatePair);
        }

        return reformatSequence(dnaSequence);
    }

    /**
     * Calculates the outer (lowest start and highest stop) coordinates
     * given a list of coordinates.
     *
     * @param allCoordinates list of all coordinates of the sequence element
     * @return               outer coordinate pair
     */
    private CoordinatePair calculateOuterCoordinates(final List<CoordinatePair> allCoordinates) {
        // If, for some reason, someone created a SequenceElement without coordinates
        // the outer coordinates will be set to 0..0
        if (allCoordinates.isEmpty()) {
            return new CoordinatePair(0, 0);
        }

        CoordinatePair firstCoordinates = allCoordinates.get(0);

        // If there is only one CoordinatePair in the list, just return that CoordinatePair
        if (allCoordinates.size() == 1) {
            return firstCoordinates;
        }

        int outerStart = firstCoordinates.getStart();
        int outerStop = firstCoordinates.getStop();

        for (CoordinatePair coordinatePair : allCoordinates) {
            // replace outerStart if a smaller start is found
            if (coordinatePair.getStart() < outerStart) {
                outerStart = coordinatePair.getStart();
            }
            // replace outerStop if a bigger stop is found
            if (coordinatePair.getStop() > outerStop) {
                outerStop = coordinatePair.getStop();
            }
        }
        return new CoordinatePair(outerStart, outerStop);
    }

    /**
     * Reformats the given (DNA/amino acid) sequence to a given
     * maximum line width.
     *
     * @param sequence the (DNA/amino acid) sequence to reformat
     * @param width    the maximum line width
     * @return         the reformatted sequence
     */
    protected final String reformatSequence(String sequence, final int width) {
        String formattedSequence = new String();

        // 'sequence' is not set to final because it is used (and changed) here
        while (sequence.length() > width) {
            formattedSequence += sequence.substring(0, width);
            formattedSequence += System.lineSeparator();
            sequence = sequence.substring(width);
        }

        formattedSequence += sequence;

        return formattedSequence;
    }

    /**
     * Works like {@link SequenceElement#reformatSequence(String,int)} except
     * the line width is always 80.
     *
     * @param sequence the (DNA/amino acid) sequence to reformat
     * @return         the reformatted sequence
     */
    protected final String reformatSequence(String sequence) {
        return reformatSequence(sequence, 80);
    }

   /**
    * Creates and returns a header for FASTA format.
    *
    * @return FASTA header
    */
    public abstract String getFastaHeader();

   /**
    * Creates a string containing details about the feature, in the following
    * format: FEATURE;TYPE;START;STOP;ORIENTATION.
    *
    * @return string containing feature details
    */
    public abstract String getFeatureDetails();

   /**
    * Gets the outer coordinates of the sequence element.
    *
    * @return outer coordinates
    */
    public final CoordinatePair getOuterCoordinates() {
        return this.outerCoordinates;
    }

   /**
    * Gets the coordinates of the sequence element.
    *
    * @return list of coordinates
    */
    public final List<CoordinatePair> getCoordinates() {
        return this.coordinates;
    }

   /**
    * Gets the orientation (FORWARD/REVERSE) of the sequence element.
    *
    * @return enum orientation
    */
    public final Orientation getOrientation() {
        return this.orientation;
    }
}
