/*
 * Copyright (c) 2015 Lonneke Scheffer [lonsch96@live.nl].
 * All rights reserved.
 *
 */

package genbank_reader;

import java.util.HashMap;

/**
 * This class represents an IUPAC pattern.
 * It contains the IUPAC pattern string, and RegEx pattern string, 
 * and a method to calculate the correct RegEx pattern given the IUPAC pattern.
 *
 * @author Lonneke Scheffer [lonsch96@live.nl]
 * @version 1.0
 */
public class IupacPattern {
    /** The original IUPAC pattern, in uppercase. */
    private final String pattern;
    /** The IUPAC pattern converted to regular expressions, in uppercase. */
    private final String regex;
    /** The hashmap used to convert IUPAC code to regular expressions. */
    private static final HashMap IUPAC_REGEX_CONVERSION = new HashMap();
    static {
        IUPAC_REGEX_CONVERSION.put("A", "A");
        IUPAC_REGEX_CONVERSION.put("T", "T");
        IUPAC_REGEX_CONVERSION.put("C", "C");
        IUPAC_REGEX_CONVERSION.put("G", "G");
        IUPAC_REGEX_CONVERSION.put("R", "[AG]");
        IUPAC_REGEX_CONVERSION.put("Y", "[CT]");
        IUPAC_REGEX_CONVERSION.put("S", "[CG]");
        IUPAC_REGEX_CONVERSION.put("W", "[AT]");
        IUPAC_REGEX_CONVERSION.put("K", "[GT]");
        IUPAC_REGEX_CONVERSION.put("M", "[AC]");
        IUPAC_REGEX_CONVERSION.put("B", "[CGT]");
        IUPAC_REGEX_CONVERSION.put("D", "[AGT]");
        IUPAC_REGEX_CONVERSION.put("H", "[ACT]");
        IUPAC_REGEX_CONVERSION.put("V", "[ACG]");
        IUPAC_REGEX_CONVERSION.put("N", "[ATCG]");
    }

    /**
     * Create a new IUPAC pattern, and determine it's corresponding RegEx pattern.
     *
     * @param pattern the given IUPAC pattern
     */
    public IupacPattern(final String pattern) {
        this.pattern = pattern.toUpperCase();
        this.regex = convertIupacToRegex(pattern.toUpperCase());
    }

    /**
     * Takes the uppercase IUPAC pattern, converts it to an uppercase RegEx pattern.
     *
     * @param iupac IUPAC pattern
     * @return      RegEx pattern
     */
    private String convertIupacToRegex(final String iupac) {
        String newRegex = "";

        // for every character in the IUPAC pattern
        for (int i = 0; i < iupac.length(); i++) {
            if (IUPAC_REGEX_CONVERSION.containsKey((iupac.charAt(i) + ""))) {
                // translate the IUPAC character to a RegEx pattern
                newRegex += IupacPattern.IUPAC_REGEX_CONVERSION.get(iupac.charAt(i) + "");
            } else {
                // throw an IllegalArgumentException if an invalid character is found
                throw new IllegalArgumentException("Your IUPAC code contained the following character: '"
                        + iupac.charAt(i) + "', this is not a valid IUPAC character!");
            }
        }
        return newRegex;
    }

    /**
     * Gets the IUPAC pattern.
     *
     * @return IUPAC pattern
     */
    public String getPattern() {
        return this.pattern;
    }

    /**
     * Gets the regular expressions pattern.
     *
     * @return RegEx pattern
     */
    public String getRegex() {
        return this.regex;
    }
    
    @Override
    public String toString() {
        return "IUPAC: " + this.getPattern() + System.lineSeparator() + "RegEx: " + this.getRegex();
    }
}
