# GenBank reader #

## Description ##

This repository contains the package genbank_reader, which is an executable that can be used to parse a GenBank file and print useful information from this file.  
Copyright (c) 2015 Lonneke Scheffer

## Requirements ##

This program uses Java 8, the used libaries are [Java SE Development Kit ](http://www.oracle.com/technetwork/java/javase/downloads/index.html) and [ApacheCommons CLI](http://commons.apache.org/proper/commons-cli/).

## Where to find everything ##

If you want to use this program without altering it, you can [download the zipped dist folder here](https://bitbucket.org/bluebasilisk/genbankreader/downloads/dist.zip).

The source files to the actual GenBank parser code, and a more detailed usage description can be found [here](https://bitbucket.org/bluebasilisk/genbankreader/src/1e167e31889d889f00232900fa2e8f1d073057b1/src/genbank_reader/?at=master).

Test data can be found in the [data folder](https://bitbucket.org/bluebasilisk/genbankreader/src/1e167e31889d889f00232900fa2e8f1d073057b1/data/?at=master) or [as a .zip in the downloads section](https://bitbucket.org/bluebasilisk/genbankreader/downloads/example_genbank_files.zip).

The generated Javadoc folder can be found [in the javadoc folder](https://bitbucket.org/bluebasilisk/genbankreader/src/1e167e31889d889f00232900fa2e8f1d073057b1/javadoc/?at=master).

And at last, [here](https://bitbucket.org/bluebasilisk/genbankreader/downloads/genbankreader.png) is an UML diagram of the GenBank parser.
